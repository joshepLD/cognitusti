package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityTareaDetalleBinding
import com.mobtk.cognitusti.model.model.Tareas
import com.mobtk.cognitusti.model.response.ResponseGuardarTarea
import com.mobtk.cognitusti.model.response.ResponseTerminarTarea
import com.mobtk.cognitusti.task.APIService
import com.mobtk.cognitusti.utilities.Utils
import kotlinx.android.synthetic.main.activity_tarea_detalle.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class TareaDetalle : AppCompatActivity() , Validator.ValidationListener  {

    var boton: Int = 0

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityTareaDetalleBinding>(this,
            R.layout.activity_tarea_detalle
        )
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tarea_detalle)

        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Tareas"
            actionBar.subtitle = "Registro"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }



        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnGuardarTarea.id -> {
                    boton = 1
                    validator!!.toValidate()
                }

                binding.btnTerminarTarea.id -> {
                    boton = 2
                    validator!!.toValidate()
                    botonTerminar()

                }
            }
        }



        // Variables que obtengo del ADAPTADOR
        val titulo = intent.getStringExtra("tarea_titulo")
        val descripcion = intent.getStringExtra("tarea_desc")
        tvHorasTotal.text = intent.getStringExtra("tarea_hrs")
        tvTareaSubtitulo.text = intent.getStringExtra("tarea_subtit")

        // Asignación de las vistas
        var titulo2 = findViewById<TextView>(R.id.tvTituloUrgente)
        var descripcion2 = findViewById<TextView>(R.id.descripcionTareaDetalle)
        var horasTotal2 = findViewById<TextView>(R.id.tvHorasTotal)
        var horasTrabajadas2 = findViewById<EditText>(R.id.EtHorasTrabajadas)

        titulo2.setText(titulo)
        descripcion2.setText(descripcion)

    }

    override fun onValidationError() {

        Toast.makeText(
            this,
            "Tienen que estar todos los campos completos correctamente",
            Toast.LENGTH_LONG
        ).show()

    }



    private fun botonTerminar(){

        val usrtarFin = intent.getStringExtra("usrtar_fin")
        var horasTrabajadas2 = intent.getStringExtra("tarea_hrs")
        var notas = intent.getStringExtra("usrtar_nota")

        //val notas = findViewById<EditText>(R.id.etNotasTarea)


        if (usrtarFin == "0"){

            EtHorasTrabajadas.isEnabled = false
            EtHorasTrabajadas.setText(horasTrabajadas2)
            etNotasTarea.isEnabled = false
            etNotasTarea.setText(notas)
            btnGuardarTarea.visibility = View.GONE
            btnTerminarTarea.visibility = View.GONE

        }

    }

    //---------------------------------------------------------

    override fun onValidationSuccess() {

        val sharedPreference = getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
        val id = sharedPreference.getString("usr_id", "")
        val usrtar = intent.getStringExtra("usrtar_id")

        //val usrtar = sharedPreference.getString("usrtar_id", "")

        Log.d("RESPUESTA", "" + id)
        Log.d("RESPUESTA", "" + usrtar)

        //val usrtar: String = contraseñaLogin.text.toString()
        val horas: String = EtHorasTrabajadas.text.toString()
        val notas: String = etNotasTarea.text.toString()



        val testValue ="501|$usrtar|$id|$horas|$notas"
        val encodeValue= Base64.encodeToString(testValue.toByteArray(), Base64.DEFAULT)
        Log.i("Base64","->${encodeValue}")

        val Value ="502|$usrtar|$id|$horas|$notas"
        val encoValue= Base64.encodeToString(Value.toByteArray(), Base64.DEFAULT)
        Log.i("Base64","->${encoValue}")


        if (Utils.verifyAvailableNetwork(this)) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)

            if(boton == 1){
                detalleTarea(encodeValue)

            } else if (boton == 2){
                Terminar(encoValue)

            }

        } else {
            Toast.makeText(
                applicationContext, "¡No cuenta con conexión a Internet!", Toast.LENGTH_SHORT).show()
        }

        //login(encodeValue)
    }

    //   -----------------------------------------------------

    private fun detalleTarea(word: String) {
        Log.d("TAG", "El valor de word" + word)
        doAsync {
            val call = getRetrofit().create(APIService::class.java).
                btnguardar("$word").execute()
            val response = call.body() as ResponseGuardarTarea
            Log.d("TAG", "Hola como estan" + response.validoGuardar)

            uiThread {

                if(response.validoGuardar == "1") {
                    initCharacter(response)

                } else {
                    showErrorDialog(response)
                }
            }

        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://35.155.161.8:8080/WSExample/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun initCharacter(response: ResponseGuardarTarea) {

        if(response.validoGuardar == "1"){

            alert("${response.mensajeGuardar}") {

                yesButton {

                    val intent = Intent(applicationContext, Tareas::class.java)
                    startActivity(intent)
                }
            }.show()
        }
    }
    private fun showErrorDialog(response: ResponseGuardarTarea) {
        alert("${response.mensajeGuardar}") {
            yesButton { }
        }.show()
    }



    // --------  BtnTerminar


    private fun Terminar(word: String) {
        Log.d("TAG", "El valor de word" + word)
        doAsync {
            val call = getRetrofit().create(APIService::class.java).
                btnterminar("$word").execute()
            val response = call.body() as ResponseTerminarTarea
            Log.d("TAG", "Hola como estan" + response.validoTerminar)

            uiThread {

                if(response.validoTerminar == "1"){
                    initCharacterTerminar(response)
                } else {
                    showErrorDialog2(response)
                }
            }

        }
    }

    private fun initCharacterTerminar(response: ResponseTerminarTarea) {

        if(response.validoTerminar == "1"){

            alert("${response.mensajeTerminar}") {
                yesButton {

                }
            }.show()
        }
    }
    private fun showErrorDialog2(response: ResponseTerminarTarea) {
        alert("${response.mensajeTerminar}") {
            yesButton { }
        }.show()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
