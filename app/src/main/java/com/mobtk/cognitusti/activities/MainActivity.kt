package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityMainBinding
import com.mobtk.cognitusti.model.response.ResponseLogin
import com.mobtk.cognitusti.task.APIService
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity(), Validator.ValidationListener{

    var ViewP:Boolean=false


    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.LbtnIniciar.id -> {
                    validator.toValidate()
                    val user = edtUsuario.text.toString().toLowerCase()
                    val pass = edtContraseña.text.toString().toLowerCase()

                    var testValue:String ="100|${user}|${pass}"
                    Log.e("word::", testValue)

                    val encodeValue= Base64.encodeToString(testValue.toByteArray(),Base64.DEFAULT)
                    Log.i("Base64","->${encodeValue}")
                    searchByName(encodeValue)
                }
                binding.TvLRegistrar.id -> {
                    val intent = Intent(this, Registro::class.java)
                    startActivity(intent)
                }
                binding.btnOlvideN.id -> {
                    val intent = Intent(this, RecuperarNip::class.java)
                    startActivity(intent)
                }
                binding.btnViewPass.id -> {
                    if(ViewP == false){
                        edtContraseña.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                       ViewP = true
                        Log.e("Pass", ViewP.toString())
                    }else{
                        edtContraseña.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        ViewP = false
                    }
                }
            }
        }
    }
    //Consumo del servicio
    private fun searchByName(word: String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).login("$word").execute()
            val response = call.body() as ResponseLogin
            uiThread {
                if(response.valido == "1") {
                    initCharacter(response)
                }else{
                    Log.e("Error",response.valido)
                    showErrorDialog()
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun initCharacter(response: ResponseLogin) {
        if(response.valido == "1"){
            val sharedPreference =  getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("usr_id", response.usuario.UId)
            editor.putString("usr_nombre",response.usuario.UNombre)
            editor.putString("usr_foto",response.usuario.UFoto)
            editor.putString("usr_correo",response.usuario.UCorreo)
            editor.commit()

            val intent = Intent(applicationContext, Home::class.java)
            startActivity(intent)
            finish()
        }

    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
    //
    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@MainActivity, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}
