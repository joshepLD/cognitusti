package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityHomeBinding



class Home : AppCompatActivity(), Validator.ValidationListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityHomeBinding>(this,R.layout.activity_home)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.TbtnCerrarS.id -> {
                    val sharedPreference =  getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
                    var editor = sharedPreference.edit()
                    editor.putString("usr_id","")
                    editor.putString("usr_id", "")
                    editor.putString("usr_nombre", "")
                    editor.putString("usr_foto", "")
                    editor.putString("usr_correo", "")
                    editor.putString("usr_app", "")
                    editor.putString("usr_apm", "")

                    editor.commit()

                    val intent = Intent(applicationContext, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                binding.HbtnCheck.id -> {
                    val intent = Intent(applicationContext, Firma::class.java)
                    startActivity(intent)
                }
                binding.HbtnPerfil.id -> {
                    val intent = Intent(applicationContext, Perfil::class.java)
                    startActivity(intent)
                }
                binding.HbtnTareas.id -> {
                    val intent = Intent(applicationContext, Tarea::class.java)
                    startActivity(intent)
                }

                binding.Notificaciones.id -> {
                    val intent = Intent(applicationContext, Notificaciones::class.java)
                    startActivity(intent)
                }
                binding.HbtnEncuesta.id -> {
                    val intent = Intent(applicationContext, Encuestas::class.java)
                    startActivity(intent)
                }
            }
        }
    }


    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@Home, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}