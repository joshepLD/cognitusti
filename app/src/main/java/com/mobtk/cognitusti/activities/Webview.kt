package com.mobtk.cognitusti.activities

import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.mobtk.cognitusti.R


class Webview : AppCompatActivity() {

    lateinit var mywebview: WebView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        mywebview = findViewById<WebView>(R.id.webview)
        mywebview.setWebViewClient(WebViewClient())
        mywebview.getSettings().setSupportZoom(true)
        mywebview.getSettings().setJavaScriptEnabled(true)
        // show backbutton and set custom title on actionbar
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Terminos y condiciones"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        val nUrl:String=intent.getStringExtra("pdf_url")
        mywebview.getSettings().setJavaScriptEnabled(true)
        mywebview.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=$nUrl")
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
