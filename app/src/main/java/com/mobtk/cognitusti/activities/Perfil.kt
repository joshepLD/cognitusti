package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.bumptech.glide.Glide
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityPerfilBinding
import com.mobtk.cognitusti.model.response.ResponsePerfil
import com.mobtk.cognitusti.task.APIService
import kotlinx.android.synthetic.main.activity_perfil.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class Perfil : AppCompatActivity(), Validator.ValidationListener {

    private val GALLERY = 1
    private val CAMERA = 2

    private var IdUsuario: String? = null
    private var correoUsr: String? = null
    private var nombreUsr: String? = null
    private var pass: String? = null

    private var mediaPath: String? = null
    private var postPath: String? = null


    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityPerfilBinding>(this, R.layout.activity_perfil)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val actionBar = supportActionBar

        if (actionBar != null) {
            actionBar.title = "Perfil"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        binding.setClickListener {
            when (it!!.id) {
                binding.btnGuardarC.id -> {
                    correoUsr = edtCorreoP.text.toString().toLowerCase()

                    var testValue: String = "400|${IdUsuario}|${nombreUsr}|${correoUsr}|${pass}"
                    Log.e("word::", testValue)

                    val encodeValue = Base64.encodeToString(testValue.toByteArray(), Base64.DEFAULT)
                    Log.i("Base64", "->${encodeValue}")
                    searchByName(encodeValue)
                }
                binding.CvEditar.id->{
                    showPictureDialog()
                }
            }
        }
        val  sharedPreference =  getSharedPreferences(
            "cognitus_ti_app",
            Context.MODE_PRIVATE
        )
        IdUsuario = sharedPreference.getString("usr_id","")
        nombreUsr = sharedPreference.getString("usr_nombre","")
        correoUsr = sharedPreference.getString("usr_correo","")
        postPath = sharedPreference.getString("usr_foto","")

        val requestManager = Glide.with(this)
        val requestBuilder = requestManager.load("http://35.155.161.8:8080/WSExample/${postPath}")
        requestBuilder.into(CvPerfil)

        tvPerfil.setText(nombreUsr)
        edtCorreoP.setText(correoUsr)


    }
    //Servicio
    private fun searchByName(word: String) {
        doAsync {
            val img = File(postPath)
            val partes = ArrayList<MultipartBody.Part>()
            partes.add(MultipartBody.Part.createFormData("word", word))
            partes.add(
                MultipartBody.Part.createFormData("archivo", img?.name, RequestBody.create(
                    MediaType.parse("image/*"), img)))
            val getResponse =
                getRetrofit().create(APIService::class.java).perfil(partes)?.execute()

            val call = getResponse?.body() as ResponsePerfil
            Log.i("Resp", "${call.validoP} ")
            Log.i("Resp", "${call.mensajeP} ")
            uiThread {
                if(call.validoP == "1") {
                    val  sharedPreference =  getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
                    var editor = sharedPreference.edit()
                    editor.putString("usr_foto", call.foptoP)
                    editor.putString("usr_correo", correoUsr)
                    editor.commit()
                    Toast.makeText(this@Perfil, "Perfil Actualizado!", Toast.LENGTH_SHORT).show()
                }else{

                    showErrorDialog()
                }
            }

        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
    //Camara
    private fun showPictureDialog(){
        val pictureDialog = AlertDialog.Builder(this)
        pictureDialog.setTitle("Foto del perfil")
        val pictureDialogItems = arrayOf("Galería", "Cámara")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    fun choosePhotoFromGallery(){
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(galleryIntent, GALLERY)
    }

    private fun takePhotoFromCamera(){
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, CAMERA)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data!!.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                try {
                    val cursor = contentResolver.query(contentURI!!, filePathColumn, null, null, null)
                    assert(cursor != null)
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    mediaPath = cursor.getString(columnIndex)
                    // Set the Image in ImageView for Previewing the Media
                    CvPerfil.setImageBitmap(BitmapFactory.decodeFile(mediaPath))
                    cursor.close()
                    postPath = mediaPath
                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@Perfil, "Hubo un error!", Toast.LENGTH_SHORT).show()
                }
            }
        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            CvPerfil!!.setImageBitmap(thumbnail)
            postPath = saveImage(thumbnail)
            Toast.makeText(this@Perfil, "Imagen Guardada", Toast.LENGTH_SHORT).show()
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
        )
        //build the directory structure
        Log.d("file", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        try {
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }
    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"
    }
    //
    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
    //Back
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
