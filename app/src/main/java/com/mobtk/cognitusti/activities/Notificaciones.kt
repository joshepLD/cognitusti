package com.mobtk.cognitusti.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.adapter.NotificacionAdapter
import com.mobtk.cognitusti.databinding.ActivityNotificacionesBinding
import com.mobtk.cognitusti.model.model.NotificacionModel
import com.mobtk.cognitusti.model.response.ResponseNotificacion
import com.mobtk.cognitusti.task.APIService
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Notificaciones : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var tarea:List<NotificacionModel>

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityNotificacionesBinding>(this, R.layout.activity_notificaciones) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val actionBar = supportActionBar

        if (actionBar != null) {
            actionBar.title = "Notificaciones"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
        tarea = ArrayList()

        val sharedPreference = getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
        val idUsuario = sharedPreference.getString("usr_id", "")
        Log.d("IDUSUARIO", "" + idUsuario)


        val testValue ="300|$idUsuario|"
        val encodeValue= Base64.encodeToString(testValue.toByteArray(),Base64.DEFAULT)
        Log.i("Base64","->${encodeValue}")
        getValueWS(encodeValue, idUsuario.toString())



    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getValueWS(word: String, idUsuario:String) {
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).notificaciones("$word").execute()
            val response = call.body() as ResponseNotificacion
            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext)
                binding.rvNotificaciones.layoutManager = linearLayoutManager
                binding.rvNotificaciones.adapter = NotificacionAdapter(response.notificacion, idUsuario)


            }

            if(response.validoNotificacion == "1"){
                initCharacter(response)
            } else {
                showErrorDialog(response)
            }
        }
    }

    private fun initCharacter(response: ResponseNotificacion) {

        if(response.validoNotificacion == "1"){

            alert("${response.mensajeNotificacion}") {
                yesButton {

                }
            }.show()
        }
    }
    private fun showErrorDialog(response: ResponseNotificacion) {
        alert("${response.mensajeNotificacion}") {
            yesButton { }
        }.show()
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
