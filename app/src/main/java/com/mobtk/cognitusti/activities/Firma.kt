package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.LocationManager
import android.os.Bundle
import android.os.Environment
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.github.gcacace.signaturepad.views.SignaturePad
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityFirmaBinding
import com.mobtk.cognitusti.model.response.ResponseCheck
import com.mobtk.cognitusti.task.APIService
import kotlinx.android.synthetic.main.activity_firma.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.util.*


class Firma : AppCompatActivity(), Validator.ValidationListener{

    private var locationManager : LocationManager? = null
    private val LOCATION_REQUEST=1500
    lateinit var urlPhoto:String

    companion object {

        val TAG = "PermissionDemo"
        private val REQUEST_PERMISSION = 2000
        private const val REQUEST_INTERNET = 200
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityFirmaBinding>(this, R.layout.activity_firma)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnCheck.id -> {
                    val signatureBitmap: Bitmap = PadFirma.transparentSignatureBitmap
                    if (addJpgSignatureToGallery(signatureBitmap)) { //Toast.makeText(SignActivity.this, "Signature saved into the Gallery", Toast.LENGTH_SHORT).show();
                        PadFirma.clear()

                        val  sharedPreference =  getSharedPreferences(
                            "cognitus_ti_app",
                            Context.MODE_PRIVATE
                        )
                        val hora = edtHora.text.toString().toLowerCase()
                        val usrId = sharedPreference.getString("usr_id","")
                        var testValue:String ="200|${usrId}|${hora}|checkInApp"
                        val encodeValue= Base64.encodeToString(testValue.toByteArray(), Base64.DEFAULT)
                        edtHora.setText("")
                        Log.e("base64F", encodeValue)
                        searchByName(encodeValue)
                    } else {
                        Toast.makeText(
                            this,
                            "Unable to store the signature",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
                binding.edtHora.id -> {
                    val date = Date()
                    val stringDate = DateFormat.getTimeInstance().format(date)
                    edtHora.setText(stringDate)
                }
                binding.btnClear.id -> {
                    PadFirma.clear()
                }
            }
        }
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Check In"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        revisaPermiso()

        PadFirma.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() { //Toast.makeText(SignActivity.this, "OnStartSigning", Toast.LENGTH_SHORT).show();
            }

            override fun onSigned() {
                btnCheck.isEnabled = true
                btnClear.isEnabled = true
            }

            override fun onClear() {
                btnCheck.isEnabled = false
                btnClear.isEnabled = false
            }
        })


    }
    //servicios

    private fun searchByName(word: String) {
        doAsync {
            val img = File(urlPhoto)
            val partes = ArrayList<MultipartBody.Part>()
            partes.add(MultipartBody.Part.createFormData("word", word))
            partes.add(MultipartBody.Part.createFormData("archivo", img?.name, RequestBody.create(MediaType.parse("image/*"), img)))
            val getResponse =
                getRetrofit().create(APIService::class.java).checkIn(partes)?.execute()

            val call = getResponse?.body() as ResponseCheck
            Log.i("Resp", "${call.validoC} ")
            Log.i("Resp", "${call.mensajeC} ")
            uiThread {
                if(call.validoC == "1") {
                        // build alert dialog
                        val dialogBuilder = AlertDialog.Builder(this@Firma)

                        // set message of alert dialog
                        dialogBuilder.setMessage("Su registro fue "+call.mensajeC)
                            // if the dialog is cancelable
                            .setCancelable(false)
                            // positive button text and action
                            .setPositiveButton("Ok", DialogInterface.OnClickListener {
                                    dialog, id -> finish()
                            })

                        // create dialog box
                        val alert = dialogBuilder.create()
                        // set title for alert dialog box
                        alert.setTitle("Check In")
                        // show alert dialog
                        alert.show()
                }else{

                    showErrorDialog()
                }
            }

        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }

    ///
    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }

    //Firma Pad
    fun revisaPermiso(){
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                REQUEST_INTERNET
            )
            Log.i(TAG, "Pide permiso")
        }

    }

    fun addJpgSignatureToGallery(signature: Bitmap): Boolean {
        var result = false
        try {
            val path =
                Environment.getExternalStorageDirectory().toString() + "/checador"
            Log.d("Files", "Path: $path")
            val fileFirm = File(path)
            fileFirm.mkdirs()
            val photo =
                File(fileFirm, "Firma.png")
            Log.d("Files", "Path: $photo")
            saveBitmapToPNG(signature, photo)

            urlPhoto = photo.toString()

            Log.e("Firma", "-->"+urlPhoto)
            result = true
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result
    }

    @Throws(IOException::class)
    fun saveBitmapToPNG(bitmap: Bitmap, photo: File) {
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(photo)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out)

        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                out?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
