package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityMainBinding
import com.mobtk.cognitusti.databinding.ActivityRecuperarNipBinding
import com.mobtk.cognitusti.model.response.ResponseLogin
import com.mobtk.cognitusti.model.response.ResponseRecuperar
import com.mobtk.cognitusti.task.APIService
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_recuperar_nip.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RecuperarNip : AppCompatActivity(), Validator.ValidationListener {

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRecuperarNipBinding>(this, R.layout.activity_recuperar_nip)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRecuperarN.id -> {
                    validator.toValidate()
                    val correo = edtCorreoN.text.toString().toLowerCase()

                    var testValue:String ="102|${correo}"
                    Log.e("word::", testValue)

                    val encodeValue= Base64.encodeToString(testValue.toByteArray(), Base64.DEFAULT)
                    Log.i("Base64","->${encodeValue}")
                    searchByName(encodeValue)
                }
            }
        }
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Olvide contraseña"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
    }
    //Consumo del servicio
    private fun searchByName(word: String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).recuperarNip("$word").execute()
            val response = call.body() as ResponseRecuperar
            uiThread {
                if(response.valido == "1") {

                    Log.e("Rsponse",response.valido)
                    Log.e("Rsponse",response.mensaje)
                        // build alert dialog
                        val dialogBuilder = AlertDialog.Builder(this@RecuperarNip)

                        // set message of alert dialog
                        dialogBuilder.setMessage(response.mensaje)
                            // if the dialog is cancelable
                            .setCancelable(false)
                            // positive button text and action
                            .setPositiveButton("ok", DialogInterface.OnClickListener {
                                    dialog, id -> finish()
                                val intent = Intent(this@RecuperarNip, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            })
                        // create dialog box
                        val alert = dialogBuilder.create()
                        // set title for alert dialog box
                        alert.setTitle("Solicitud Exitosa")
                        // show alert dialog
                        alert.show()
                }else{
                    Log.e("Error",response.valido)
                    showErrorDialog()
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }
    //
    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }
}
