package com.mobtk.cognitusti.activities

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.adapter.TareasAdapter
import com.mobtk.cognitusti.databinding.ActivityTareasBinding
import com.mobtk.cognitusti.model.model.Tareas
import com.mobtk.cognitusti.model.response.ResponseTareas
import com.mobtk.cognitusti.task.APIService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Tarea : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var tarea:List<Tareas>

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityTareasBinding>(this, R.layout.activity_tareas) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Tareas"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }


        tarea = ArrayList()

        val sharedPreference = getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
        val id = sharedPreference.getString("usr_id", "")



        Log.d("BASE", "RESPUESTA" + id)
        val testValue ="500|$id|"
        val encodeValue= Base64.encodeToString(testValue.toByteArray(),Base64.DEFAULT)
        Log.i("Base64","->${encodeValue}")
        getValueWS(encodeValue)

    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun getValueWS(word: String) {
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).tareaApi("$word").execute()
            val response = call.body() as ResponseTareas
            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext)
                binding.rvTareas.layoutManager = linearLayoutManager
                binding.rvTareas.adapter = TareasAdapter(response.tareas)

            }
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

