package com.mobtk.cognitusti.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.adapter.EncuestaAdapter
import com.mobtk.cognitusti.adapter.TareasAdapter
import com.mobtk.cognitusti.databinding.ActivityEncuestasBinding
import com.mobtk.cognitusti.model.model.Encuesta
import com.mobtk.cognitusti.model.response.ResponseEncuesta
import com.mobtk.cognitusti.task.APIService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Encuestas : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var encuesta:List<Encuesta>

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityEncuestasBinding>(this, R.layout.activity_encuestas) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.title = "Encuestas"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        encuesta = ArrayList()



        val testValue ="600"
        var encodeValue= Base64.encodeToString(testValue.toByteArray(), Base64.DEFAULT)
        encodeValue=encodeValue.replace("\\\n".toRegex(),"")
        Log.i("Base64","->${encodeValue}")
        getValueWS(encodeValue)

    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun getValueWS(word: String) {
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).encuesta("$word").execute()
            val response = call.body() as ResponseEncuesta
            uiThread {
                linearLayoutManager = LinearLayoutManager(applicationContext)
                binding.rvEncuesta.layoutManager = linearLayoutManager
                binding.rvEncuesta.adapter = EncuestaAdapter(response.Encuesta)

            }
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
