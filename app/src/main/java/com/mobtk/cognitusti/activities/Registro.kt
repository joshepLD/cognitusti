package com.mobtk.cognitusti.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.util.Base64
import android.util.Log
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import br.com.ilhasoft.support.validation.Validator
import com.mobtk.cognitusti.R
import com.mobtk.cognitusti.databinding.ActivityMainBinding
import com.mobtk.cognitusti.databinding.ActivityRegistroBinding
import com.mobtk.cognitusti.model.response.ResponseLogin
import com.mobtk.cognitusti.model.response.ResponseRegistro
import com.mobtk.cognitusti.model.response.ResponseTerminos
import com.mobtk.cognitusti.task.APIService
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registro.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Registro : AppCompatActivity(), Validator.ValidationListener {

    lateinit var urlPdf:String

    var ViewP:Boolean=false
    var ViewP2:Boolean=false

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityRegistroBinding>(this, R.layout.activity_registro)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)
        val actionBar = supportActionBar
        val validator: Validator = Validator(binding)
        validator.setValidationListener(this)

        //Consumo de terminos y condiciones

        var testValue:String ="99"
        Log.e("word::", testValue)

        val encodeValue= Base64.encodeToString(testValue.toByteArray(),Base64.DEFAULT)
        Log.i("Base64","->${encodeValue}")


        doAsync {
            val call = getRetrofit().create(APIService::class.java).terminos("$encodeValue").execute()
            val response = call.body() as ResponseTerminos
            uiThread {
                if(response.ValidoP == "1") {
                    Log.e("Terminos", response.Pdf)
                    urlPdf = "http://35.155.161.8:8080/WSExample/${response.Pdf}"
                }else{
                    Log.e("Error",response.ValidoP)
                    showErrorDialog()
                }
            }
        }
        //
        binding.setClickListener {
            when (it!!.id) {
                binding.btnRegistrar.id -> {
                    validator.toValidate()
                    val user = edtUsuarioR.text.toString().toLowerCase()
                    val correo = edtCorreoR.text.toString().toLowerCase()
                    val pass = edtContraseñaR.text.toString().toLowerCase()

                    var testValue:String ="101|${user}|${correo}|${pass}"
                    Log.e("word::", testValue)

                    val encodeValue= Base64.encodeToString(testValue.toByteArray(),Base64.DEFAULT)
                    Log.i("Base64","->${encodeValue}")
                    searchByName(encodeValue)
                }
                binding.btnTerminos.id -> {
                    val intent = Intent(this, Webview::class.java)
                    Log.e("Pdf", urlPdf)
                    intent.putExtra("pdf_url", ""+urlPdf )
                    startActivity(intent)
                }
                binding.btnViewU.id -> {
                    if(ViewP == false){
                        edtContraseñaR.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD);
                        ViewP = true
                        Log.e("Pass", ViewP.toString())
                    }else{
                        edtContraseñaR.setInputType(InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD)
                        ViewP = false
                    }
                }
                binding.btnViewD.id -> {
                    if(ViewP2 == false){
                        edtRContraseñaR.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                        ViewP2 = true
                    }else{
                        edtRContraseñaR.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD)
                        ViewP2 = false
                    }
                }
            }
        }

        if (actionBar != null) {
            actionBar.title = "Registro"
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }
    }
    //service
    private fun searchByName(word: String) {
        doAsync {
            val call = getRetrofit().create(APIService::class.java).registro("$word").execute()
            val response = call.body() as ResponseRegistro
            uiThread {
                if(response.valido == "1") {
                    initCharacter(response)
                }else{
                    Log.e("Error",response.valido)
                    showErrorDialog()
                }
            }
        }
    }
    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(getString(R.string.ws_url_login))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
    private fun initCharacter(response: ResponseRegistro) {
        if(response.valido == "1"){
            Log.e("Registro", response.valido)
            Log.e("Registro", response.registro.Nombre)
            Log.e("Registro", response.registro.Id)
            val sharedPreference =  getSharedPreferences("cognitus_ti_app", Context.MODE_PRIVATE)
            var editor = sharedPreference.edit()
            editor.putString("usr_id", response.registro.Id)
            editor.putString("usr_nombre",response.registro.Nombre)
            editor.putString("usr_foto",response.registro.Foto)
            editor.putString("usr_correo",response.registro.Correo)
            editor.commit()

            val intent = Intent(applicationContext, Home::class.java)
            startActivity(intent)
            finish()
        }

    }

    private fun showErrorDialog() {
        alert("Ha ocurrido un error, inténtelo de nuevo.") {
            yesButton { }
        }.show()
    }

    //
    override fun onValidationSuccess() {
        Toast.makeText(this,"Todo ok ", Toast.LENGTH_LONG).show()
    }

    override fun onValidationError() {
        Toast.makeText(this@Registro, "Dados inválidos!", Toast.LENGTH_SHORT).show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
