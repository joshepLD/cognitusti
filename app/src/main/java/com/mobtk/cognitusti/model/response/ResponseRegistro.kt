package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Login
import com.mobtk.cognitusti.model.model.Registro

class ResponseRegistro (@SerializedName("valido") var valido:String,
                        @SerializedName("usuario") var registro: Registro,
                        @SerializedName("mensaje") var mensaje:String)