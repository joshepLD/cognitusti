package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CheckIn (
    @SerializedName("ck_tiemporeal")@Expose val tiempoR:String?,
    @SerializedName("ck_firmaurl")@Expose val urlFirma:String?,
    @SerializedName("ck_id")@Expose val idC:String?,
    @SerializedName("ck_hraenv")@Expose val hraenv:String?
)