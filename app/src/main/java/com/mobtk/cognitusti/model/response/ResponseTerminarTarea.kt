package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName

data class ResponseTerminarTarea (@SerializedName("valido") val validoTerminar: String,
                             @SerializedName("mensaje") val mensajeTerminar: String)