package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class  Tareas (@SerializedName("tarea_subtit") @Expose val tarea: String?=null,
              @SerializedName ("usrtar_nota") @Expose val notasTarea: String?=null,
              @SerializedName ("usrtar_horas") @Expose val horasUsuario: String?=null,
              @SerializedName ("tarea_titulo") @Expose val tituloTarea: String?=null,
              @SerializedName ("usrtar_id") @Expose val idStarTarea: String?=null,
              @SerializedName ("tarea_hrs") @Expose val horasTarea: String?=null,
              @SerializedName ("usrtar_fin") @Expose val horasFinTarea: String?=null,
              @SerializedName ("tarea_desc") @Expose val descTarea: String?=null,
              @SerializedName ("usrtar_status") @Expose val statusTarea: String?=null)