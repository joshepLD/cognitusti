package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Encuesta
import com.mobtk.cognitusti.model.model.Tareas

data class ResponseEncuesta (@SerializedName("encuesta")@Expose val Encuesta: ArrayList<Encuesta>,
                             @SerializedName ("valido") val validoEncuesta: String,
                             @SerializedName ("mensaje") val mensajeEncuesta: String)