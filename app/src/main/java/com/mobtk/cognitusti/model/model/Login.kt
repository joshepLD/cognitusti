package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Login (
    @SerializedName("usr_rutafoto")@Expose val UFoto:String?,
    @SerializedName("usr_email")@Expose val UCorreo:String?,
    @SerializedName("usr_id")@Expose val UId:String?,
    @SerializedName("usr_nombre")@Expose val UNombre:String?
)