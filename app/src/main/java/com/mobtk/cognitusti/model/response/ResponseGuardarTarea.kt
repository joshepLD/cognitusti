package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.GuardarTarea

data class ResponseGuardarTarea (@SerializedName("valido") val validoGuardar: String,
                            @SerializedName ("mensaje") val mensajeGuardar: String,
                            @SerializedName ("tareas")  @Expose val tareasGuardar: GuardarTarea?)