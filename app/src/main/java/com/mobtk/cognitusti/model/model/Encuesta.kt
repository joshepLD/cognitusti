package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.SerializedName

data class Encuesta (@SerializedName("respuestas") val resp: List<respuestas>,
                     @SerializedName ("cpreg_id") val cpId: String?,
                     @SerializedName ("cpreg_tipo") val cpTipo: String?,
                     @SerializedName ("cpreg_titulo") val cpTitulo: String?)