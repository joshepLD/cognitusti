package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName

data class ResponseNotificacionNoLeida (@SerializedName("valido") val validoNotifiNoLeida: String,
                                        @SerializedName ("mensaje") val mensajeNotifiNoLeida: String)