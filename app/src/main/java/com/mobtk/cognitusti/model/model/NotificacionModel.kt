package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.SerializedName

data class NotificacionModel (@SerializedName("not_id") val id:String?=null,
                              @SerializedName ("not_titulo") val tituloNotificacion:String?=null,
                              @SerializedName ("not_update") val actualizar: String?=null,
                              @SerializedName ("not_desc") val descNotificacion: String?=null,
                              @SerializedName ("not_status") val statusNotificacion: String?=null)