package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Tareas

data class ResponseTareas (@SerializedName("valido") val validoTarea: String,
                           @SerializedName ("mensaje") val mensajeTarea: String,
                           @SerializedName ("tareas")  @Expose val tareas: ArrayList<Tareas>)