package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Registro (
    @SerializedName("usu_id")@Expose val    Id:String?,
    @SerializedName("usr_rutafoto")@Expose val Foto:String?,
    @SerializedName("usr_email")@Expose val Correo:String?,
    @SerializedName("usr_nombre")@Expose val Nombre:String?
)