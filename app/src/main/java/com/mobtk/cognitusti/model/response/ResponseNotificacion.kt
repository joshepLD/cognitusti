package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.NotificacionModel

class ResponseNotificacion (@SerializedName ("notificaciones") @Expose val notificacion:  ArrayList<NotificacionModel>,
                            @SerializedName("valido") val validoNotificacion: String,
                            @SerializedName ("mensaje") val mensajeNotificacion: String)