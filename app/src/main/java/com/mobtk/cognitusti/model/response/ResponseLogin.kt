package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Login

class ResponseLogin (@SerializedName("valido") var valido:String,
                     @SerializedName("usuario") var usuario: Login,
                     @SerializedName("mensaje") var mensaje:String)