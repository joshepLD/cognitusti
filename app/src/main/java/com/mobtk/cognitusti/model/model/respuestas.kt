package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.SerializedName

data class respuestas (@SerializedName ("cresp_desc") val cpId: String?,
                       @SerializedName ("cresp_id") val cpTipo: String?)