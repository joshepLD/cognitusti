package com.mobtk.cognitusti.model.model

import com.google.gson.annotations.SerializedName

data class GuardarTarea (@SerializedName("tarea_subtit") val subTituloTareGuardar: String?=null,
                    @SerializedName ("usrtar_nota") val notasGuardar: String?=null,
                    @SerializedName ("usrtar_horas") val horasGuardar: String?=null,
                    @SerializedName ("tarea_titulo") val tituloGuardar: String?=null,
                    @SerializedName ("usrtar_id") val idUsuarioGuardar: String?=null,
                    @SerializedName ("tarea_hrs") val tareaHorasGuardar: String?=null,
                    @SerializedName ("usrtar_fin") val tareaFinGuardar: String?=null,
                    @SerializedName ("tarea_desc") val descGuardar: String?=null,
                    @SerializedName ("usrtar_status") val statusGuardar: String?=null)