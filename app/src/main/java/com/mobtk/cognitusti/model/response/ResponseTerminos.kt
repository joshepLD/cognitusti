package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Login

class ResponseTerminos (@SerializedName("url_pdf") var Pdf:String,
                        @SerializedName("valido") var ValidoP:String,
                        @SerializedName("mensaje") var MensajeP:String)