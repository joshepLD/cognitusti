package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Login

class ResponsePerfil (@SerializedName("ruta_foto") var foptoP:String,
                      @SerializedName("valido") var validoP: String,
                      @SerializedName("mensaje") var mensajeP:String)