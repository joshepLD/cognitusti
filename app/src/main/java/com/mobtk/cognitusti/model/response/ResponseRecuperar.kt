package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.Login

class ResponseRecuperar (@SerializedName("valido") var valido:String,
                         @SerializedName("mensaje") var mensaje:String)