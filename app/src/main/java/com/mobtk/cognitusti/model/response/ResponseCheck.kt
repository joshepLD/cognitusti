package com.mobtk.cognitusti.model.response

import com.google.gson.annotations.SerializedName
import com.mobtk.cognitusti.model.model.CheckIn
import com.mobtk.cognitusti.model.model.Login

class ResponseCheck (@SerializedName("valido") var validoC:String,
                     @SerializedName("registro_ck") var registro_ck: CheckIn,
                     @SerializedName("mensaje") var mensajeC:String)