package com.mobtk.cognitusti.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.cognitusti.databinding.ItemEncuestaBinding
import com.mobtk.cognitusti.model.model.Encuesta

class EncuestaAdapter (private val items: List<Encuesta>) : RecyclerView.Adapter<EncuestaAdapter.ViewHolder>() {
    private var contexto: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto=parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemEncuestaBinding.inflate(inflater)
        return ViewHolder(binding)

    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(items[position])
        holder.bind(items[position])
        if(items[position].cpTipo.equals("1"))
            holder.binding.preguntaText.visibility= View.VISIBLE
        if(items[position].cpTipo.equals("2"))
            holder.binding.stars.visibility=View.VISIBLE
        if(items[position].cpTipo.equals("3"))
            holder.binding.yn.visibility=View.VISIBLE
        if(items[position].cpTipo.equals("4"))
            holder.binding.multiple.visibility=View.VISIBLE

        holder.binding.setClickListener {
            when (it!!.id) {
                holder.binding.Item.id -> {

                }
            }
        }
    }
    inner class ViewHolder(val binding: ItemEncuestaBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(encuestaModel: Encuesta) {

                binding.elemento = encuestaModel
                binding.executePendingBindings()


        }
    }
}
