package com.mobtk.cognitusti.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.cognitusti.activities.TareaDetalle
import com.mobtk.cognitusti.databinding.ItemTareasBinding
import com.mobtk.cognitusti.model.model.Tareas

class TareasAdapter(private val items: ArrayList<Tareas>) : RecyclerView.Adapter<TareasAdapter.ViewHolder>() {
    private var contexto: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto=parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemTareasBinding.inflate(inflater)
        return ViewHolder(binding)

    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {




        holder.bind(items[position])
        holder.binding.setClickListener {
            when (it!!.id) {
                holder.binding.tareaUrgente.id -> {

                    val intent = Intent(holder.itemView.context, TareaDetalle::class.java)
                    intent.putExtra("tarea_subtit", items[position].tarea)
                    intent.putExtra("usrtar_nota", items[position].notasTarea)
                    intent.putExtra("usrtar_horas", items[position].horasUsuario)
                    intent.putExtra("tarea_titulo", items[position].tituloTarea)
                    intent.putExtra("usrtar_id", items[position].idStarTarea)
                    intent.putExtra("tarea_hrs", items[position].horasTarea)
                    intent.putExtra("usrtar_fin", items[position].horasFinTarea)
                    intent.putExtra("tarea_desc", items[position].descTarea)
                    intent.putExtra("usrtar_status", items[position].statusTarea)
                    holder.itemView.context.startActivity(intent)
                }

                holder.binding.tareaNormal.id -> {

                    val intent = Intent(holder.itemView.context, TareaDetalle::class.java)
                    intent.putExtra("tarea_subtit", items[position].tarea)
                    intent.putExtra("usrtar_nota", items[position].notasTarea)
                    intent.putExtra("usrtar_horas", items[position].horasUsuario)
                    intent.putExtra("tarea_titulo", items[position].tituloTarea)
                    intent.putExtra("usrtar_id", items[position].idStarTarea)
                    intent.putExtra("tarea_hrs", items[position].horasTarea)
                    intent.putExtra("usrtar_fin", items[position].horasFinTarea)
                    intent.putExtra("tarea_desc", items[position].descTarea)
                    intent.putExtra("usrtar_status", items[position].statusTarea)
                    holder.itemView.context.startActivity(intent)


                }
            }
        }
        //UtilisGeneral.loadImage(contexto,holder.binding.ivFotoNot, items[position].notImg.toString(), 1)
    }
    inner class ViewHolder(val binding: ItemTareasBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(tareaModel: Tareas) {

            if (tareaModel.statusTarea.toString() == "1"){
                binding.tareaUrgente.visibility = View.VISIBLE
                binding.tareaNormal.visibility = View.GONE
                binding.elemento = tareaModel
                binding.executePendingBindings()

            }

            if (tareaModel.statusTarea.toString() == "2"){
                binding.tareaUrgente.visibility = View.GONE
                binding.tareaNormal.visibility = View.VISIBLE
                binding.elemento2 = tareaModel
                binding.executePendingBindings()

            }

        }
    }
}