package com.mobtk.cognitusti.adapter

import android.content.Context
import android.content.Intent
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobtk.cognitusti.activities.Notificaciones
import com.mobtk.cognitusti.databinding.ItemNotificacionesBinding
import com.mobtk.cognitusti.model.model.NotificacionModel
import com.mobtk.cognitusti.model.response.ResponseNotificacionNoLeida
import com.mobtk.cognitusti.task.APIService
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NotificacionAdapter (private val items: List<NotificacionModel>, private val idusuario: String) : RecyclerView.Adapter<NotificacionAdapter.ViewHolder>() {
    private var contexto: Context? =null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        contexto=parent.context
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemNotificacionesBinding.inflate(inflater)
        return ViewHolder(binding)

    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(items[position])
        holder.binding.setClickListener {
            when (it!!.id) {
                holder.binding.tvNotificacionesNoLeida.id -> {


                    val idnotificacion:String = items[position].id.toString()
                    val testValue ="301|$idnotificacion|$idusuario"
                    val encodeValue= Base64.encodeToString(testValue.toByteArray(), Base64.DEFAULT)
                    //Log.i("Base64","->${encodeValue}")
                    //Log.d("Holaaaaaaaa", "" + idnotificacion)
                    getValueWS(encodeValue)

                    //val intent = Intent(holder.itemView.context, Notificaciones::class.java)
                    //holder.itemView.context.startActivity(intent)
                }
            }
        }
        //UtilisGeneral.loadImage(contexto,holder.binding.ivFotoNot, items[position].notImg.toString(), 1)
    }
    inner class ViewHolder(val binding: ItemNotificacionesBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(notificacionModel: NotificacionModel) {


            if (notificacionModel.statusNotificacion.toString() == "0"){
                binding.tvNotificacionesLeida.visibility = View.VISIBLE
                binding.tvNotificacionesNoLeida.visibility = View.GONE
                binding.elemento = notificacionModel
                binding.executePendingBindings()
            }

            if (notificacionModel.statusNotificacion.toString() == "1"){
                binding.tvNotificacionesNoLeida.visibility = View.VISIBLE
                binding.tvNotificacionesLeida.visibility = View.GONE
                binding.elemento2 = notificacionModel
                binding.executePendingBindings()

            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("http://35.155.161.8:8080/WSExample/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getValueWS(word: String) {
        doAsync {
            val call =
                getRetrofit().create(APIService::class.java).notificacionNoLeida("$word").execute()
            val response = call.body() as ResponseNotificacionNoLeida
            uiThread {
                if(response.validoNotifiNoLeida == "1"){
                    initCharacter(response)
                } else {
                    showErrorDialog(response)
                }

            }
        }
    }

    private fun initCharacter(response: ResponseNotificacionNoLeida) {

        if(response.validoNotifiNoLeida == "1"){

            contexto?.alert("${response.mensajeNotifiNoLeida}") {
                yesButton {

                    val intent = Intent(contexto, Notificaciones::class.java)
                    contexto?.startActivity(intent)
                    (contexto as Notificaciones).finish()


                    //valido, mensaje iniciar la activity

                }
            }?.show()
        }
    }
    private fun showErrorDialog(response: ResponseNotificacionNoLeida) {
        contexto?.alert("${response.mensajeNotifiNoLeida}") {
            yesButton { }
        }?.show()
    }



}