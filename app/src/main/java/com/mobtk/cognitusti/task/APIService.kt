package com.mobtk.cognitusti.task

import com.mobtk.cognitusti.model.response.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @GET("GeneralData?")
    fun login(@Query ("word") correo:String): Call<ResponseLogin>

    @GET("GeneralData?")
    fun registro(@Query ("word") correo:String): Call<ResponseRegistro>

    @GET("GeneralData?")
    fun recuperarNip(@Query ("word") correo:String): Call<ResponseRecuperar>

    @GET("GeneralData?")
    fun terminos(@Query ("word") correo:String): Call<ResponseTerminos>

    @Multipart
    @POST("GeneralData?")
    fun checkIn(
        @Part partMap: List<MultipartBody.Part> ): Call<ResponseCheck>

    @Multipart
    @POST("GeneralData?")
    fun perfil(
        @Part partMap: List<MultipartBody.Part> ): Call<ResponsePerfil>

    @GET("GeneralData")
    fun tareaApi (@Query ("word") tareasTodo: String): Call<ResponseTareas>

    @GET("GeneralData")
    fun btnguardar (@Query ("word") guardar:String): Call<ResponseGuardarTarea>

    @GET("GeneralData")
    fun btnterminar (@Query ("word") terminar:String): Call<ResponseTerminarTarea>

    @GET("GeneralData")
    fun notificaciones (@Query ("word") notifica: String): Call<ResponseNotificacion>

    @GET("GeneralData")
    fun notificacionNoLeida (@Query ("word") notifica: String): Call<ResponseNotificacionNoLeida>

    @GET("GeneralData?")
    fun encuesta(@Query ("word") encuesta:String): Call<ResponseEncuesta>
}
//photoFile?.crearMultiparte